# taskfiles

My collection of taskfiles.

## Requirements

> ℹ️ In order to ease dependency management and `Taskfile.yml` composition,
>
> 1. 🐳 Tasks will **run tools in docker** when applicable and pertinent
> 2. ⚠️ Tasks will **warn if a dependency is missing** before running
> 3. 🔓 There is **no interdependency between the taskfiles** of this repository

Here is a list of the required tools:

- **mandatory**:
  [go-task](https://taskfile.dev/installation/)
- **a limited list of tools needed in many taskfiles**:
  [docker](https://docs.docker.com/engine/install/),
  [curl](https://everything.curl.dev/install),
  [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and
  [jq](https://jqlang.github.io/jq/download/)
- in dedicated taskfiles:
  [dprint](https://dprint.dev/install/) and [poetry](https://python-poetry.org/docs/#installation) ...

## Getting started

### Create a new `Taskfile.yml` for your project

```bash
cd path/to/your/project

# Enable Remote Taskfile Experiment
echo "TASK_X_REMOTE_TASKFILES=1" > .env

# Download default example Taskfile
curl -o Taskfile.yml https://gitlab.com/gp3t1/taskfiles/-/raw/main/examples/Taskfile.yml

# Uncomment included taskfiles if needed
...
```

### Or update an existing `Taskfile.yml`

```yaml
includes:
  # Common tasks
  git: https://gitlab.com/gp3t1/taskfiles/-/raw/main/generic/git.Taskfile.yml
  gocloc: https://gitlab.com/gp3t1/taskfiles/-/raw/main/generic/gocloc.Taskfile.yml
  # ...

  # langage-based tasks (uncomment to enable)
  shellcheck: https://gitlab.com/gp3t1/taskfiles/-/raw/main/lang/shell/shellcheck.Taskfile.yml
  yamllint: https://gitlab.com/gp3t1/taskfiles/-/raw/main/lang/yaml/yamllint.Taskfile.yml
  # ...
```

## Some available tasks

![Some available tasks](docs/assets/list-tasks.gif){width=66%}
